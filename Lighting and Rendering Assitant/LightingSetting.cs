﻿using UnityEngine;
using Config;

namespace LightingRenderingAssitant
{
    public class LightingSetting : BaseSystem
    {
        public LightingSetting(string elementName) : base(elementName)
        {
        }

        public override void Init()
        {
            AmbientMode = -1;
            AmbientIntensity = -1f;
            AmbientSkyColor = Color.clear;
            AmbientEquatorColor = Color.clear;
            AmbientGoundColor = Color.clear;
            ReflectionIntensity = -1f;
            FrontLightIntensity = -1f;
            FrontLightColor = Color.clear;
            BackLightIntensity = -1f;
            BackLightColor = Color.clear;
        }

        public int AmbientMode;
        public float AmbientIntensity;
        public Color AmbientSkyColor;
        public Color AmbientEquatorColor;
        public Color AmbientGoundColor;
        public float ReflectionIntensity;
        public float FrontLightIntensity;
        public Color FrontLightColor;
        public float BackLightIntensity;
        public Color BackLightColor;
    }

}
