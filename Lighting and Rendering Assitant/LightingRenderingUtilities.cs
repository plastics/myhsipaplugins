﻿using UnityEngine;
using System;

namespace LightingRenderingAssitant
{
    public static class LightingRenderingUtilities
    {
        public static void GatherLightInfo()
        {

            Console.WriteLine("Ambient Mode: " + RenderSettings.ambientMode.ToString());
            Console.WriteLine("Ambient Intensity: " + RenderSettings.ambientIntensity.ToString());
            Console.WriteLine("Ambient Sky Color: " + RenderSettings.ambientSkyColor.ToString());
            Console.WriteLine("Ambient Equator Color: " + RenderSettings.ambientEquatorColor.ToString());
            Console.WriteLine("Ambient Ground Color: " + RenderSettings.ambientGroundColor.ToString());
            Console.WriteLine("Renflection Intensity: " + RenderSettings.reflectionIntensity.ToString());
            Light[] lights;
            lights = UnityEngine.Object.FindObjectsOfType<Light>();
            foreach (Light light in lights)
            {
                Console.WriteLine(light.name.ToString() + " Intensity: " + light.intensity.ToString() + " Color " + light.color.ToString());
            }

        }
        public static void SetLighting(LightingSetting Lightsettings)
        {
            if (Lightsettings.AmbientMode < 0)
                Lightsettings.AmbientMode = (int)RenderSettings.ambientMode;
            else
                RenderSettings.ambientMode = (UnityEngine.Rendering.AmbientMode)Lightsettings.AmbientMode;
            if (Lightsettings.AmbientIntensity < 0)
                Lightsettings.AmbientIntensity = RenderSettings.ambientIntensity;
            else
                RenderSettings.ambientIntensity = Lightsettings.AmbientIntensity;
            if (Lightsettings.AmbientMode == 0)
                DynamicGI.UpdateEnvironment();

            if (Lightsettings.AmbientSkyColor == Color.clear)
                Lightsettings.AmbientSkyColor = RenderSettings.ambientSkyColor;
            else
                RenderSettings.ambientSkyColor = Lightsettings.AmbientSkyColor;
            if (Lightsettings.AmbientEquatorColor == Color.clear)
                Lightsettings.AmbientEquatorColor = RenderSettings.ambientEquatorColor;
            else
                RenderSettings.ambientEquatorColor = Lightsettings.AmbientEquatorColor;
            if (Lightsettings.AmbientGoundColor == Color.clear)
                Lightsettings.AmbientGoundColor = RenderSettings.ambientGroundColor;
            else
                RenderSettings.ambientGroundColor = Lightsettings.AmbientGoundColor;

            if (Lightsettings.ReflectionIntensity < 0)
                Lightsettings.ReflectionIntensity = RenderSettings.reflectionIntensity;
            else
                RenderSettings.reflectionIntensity = Lightsettings.ReflectionIntensity;
            Light[] lights;
            lights = UnityEngine.Object.FindObjectsOfType<Light>();
            foreach (Light light in lights)
            {
                if (light.name == "DirectionalFront" || light.name == "Directional Front")
                {
                    if (Lightsettings.FrontLightIntensity < 0)
                        Lightsettings.FrontLightIntensity = light.intensity;
                    else
                        light.intensity = Lightsettings.FrontLightIntensity;
                    if (Lightsettings.FrontLightColor == Color.clear)
                        Lightsettings.FrontLightColor = light.color;
                    else
                        light.color = Lightsettings.FrontLightColor;
                }
                else if (light.name == "DirectionalBack" || light.name == "Directional Back")
                {
                    if (Lightsettings.BackLightIntensity < 0)
                        Lightsettings.BackLightIntensity = light.intensity;
                    else
                        light.intensity = Lightsettings.BackLightIntensity;
                    if (Lightsettings.BackLightColor == Color.clear)
                        Lightsettings.BackLightColor = light.color;
                    else
                        light.color = Lightsettings.BackLightColor;
                }
                else if (light.name == "DirectionalFrontMap" && Lightsettings.FrontLightColor != Color.clear && Lightsettings.FrontLightIntensity >= 0f)
                {
                    light.intensity = Lightsettings.FrontLightIntensity;
                    light.color = Lightsettings.FrontLightColor;
                }

            }
        }

        public static void SetLightingKey(LightingSettingKey Lightsettings)
        {
            if (Lightsettings.AmbientMode < 0)
                Lightsettings.AmbientMode = (int)RenderSettings.ambientMode;
            else
                RenderSettings.ambientMode = (UnityEngine.Rendering.AmbientMode)Lightsettings.AmbientMode;
            if (Lightsettings.AmbientIntensity < 0)
                Lightsettings.AmbientIntensity = RenderSettings.ambientIntensity;
            else
                RenderSettings.ambientIntensity = Lightsettings.AmbientIntensity;
            if (Lightsettings.AmbientMode == 0)
                DynamicGI.UpdateEnvironment();

            if (Lightsettings.AmbientSkyColor == Color.clear)
                Lightsettings.AmbientSkyColor = RenderSettings.ambientSkyColor;
            else
                RenderSettings.ambientSkyColor = Lightsettings.AmbientSkyColor;
            if (Lightsettings.AmbientEquatorColor == Color.clear)
                Lightsettings.AmbientEquatorColor = RenderSettings.ambientEquatorColor;
            else
                RenderSettings.ambientEquatorColor = Lightsettings.AmbientEquatorColor;
            if (Lightsettings.AmbientGoundColor == Color.clear)
                Lightsettings.AmbientGoundColor = RenderSettings.ambientGroundColor;
            else
                RenderSettings.ambientGroundColor = Lightsettings.AmbientGoundColor;

            if (Lightsettings.ReflectionIntensity < 0)
                Lightsettings.ReflectionIntensity = RenderSettings.reflectionIntensity;
            else
                RenderSettings.reflectionIntensity = Lightsettings.ReflectionIntensity;
            Light[] lights;
            lights = UnityEngine.Object.FindObjectsOfType<Light>();
            foreach (Light light in lights)
            {
                if (light.name == "DirectionalFront" || light.name == "Directional Front")
                {
                    if (Lightsettings.FrontLightIntensity < 0)
                        Lightsettings.FrontLightIntensity = light.intensity;
                    else
                        light.intensity = Lightsettings.FrontLightIntensity;
                    if (Lightsettings.FrontLightColor == Color.clear)
                        Lightsettings.FrontLightColor = light.color;
                    else
                        light.color = Lightsettings.FrontLightColor;
                }
                else if (light.name == "DirectionalBack" || light.name == "Directional Back")
                {
                    if (Lightsettings.BackLightIntensity < 0)
                        Lightsettings.BackLightIntensity = light.intensity;
                    else
                        light.intensity = Lightsettings.BackLightIntensity;
                    if (Lightsettings.BackLightColor == Color.clear)
                        Lightsettings.BackLightColor = light.color;
                    else
                        light.color = Lightsettings.BackLightColor;
                }
                else if (light.name == "DirectionalFrontMap" && Lightsettings.FrontLightColor != Color.clear && Lightsettings.FrontLightIntensity >= 0f)
                {
                    light.intensity = Lightsettings.FrontLightIntensity;
                    light.color = Lightsettings.FrontLightColor;
                }
                else if (light.name == "Directional Key")
                {
                    if (Lightsettings.KeyLightIntensity < 0)
                        Lightsettings.KeyLightIntensity = light.intensity;
                    else
                        light.intensity = Lightsettings.KeyLightIntensity;
                    if (Lightsettings.KeyLightColor == Color.clear)
                        Lightsettings.KeyLightColor = light.color;
                    else
                        light.color = Lightsettings.KeyLightColor;
                }

            }
        }

        public static void SetLightingLobby(LobbyLight Lightsettings)
        {
            if (Lightsettings.AmbientMode < 0)
                Lightsettings.AmbientMode = (int)RenderSettings.ambientMode;
            else
                RenderSettings.ambientMode = (UnityEngine.Rendering.AmbientMode)Lightsettings.AmbientMode;
            if (Lightsettings.AmbientIntensity < 0)
                Lightsettings.AmbientIntensity = RenderSettings.ambientIntensity;
            else
                RenderSettings.ambientIntensity = Lightsettings.AmbientIntensity;
            if (Lightsettings.AmbientMode == 0)
                DynamicGI.UpdateEnvironment();

            if (Lightsettings.AmbientSkyColor == Color.clear)
                Lightsettings.AmbientSkyColor = RenderSettings.ambientSkyColor;
            else
                RenderSettings.ambientSkyColor = Lightsettings.AmbientSkyColor;
            if (Lightsettings.AmbientEquatorColor == Color.clear)
                Lightsettings.AmbientEquatorColor = RenderSettings.ambientEquatorColor;
            else
                RenderSettings.ambientEquatorColor = Lightsettings.AmbientEquatorColor;
            if (Lightsettings.AmbientGoundColor == Color.clear)
                Lightsettings.AmbientGoundColor = RenderSettings.ambientGroundColor;
            else
                RenderSettings.ambientGroundColor = Lightsettings.AmbientGoundColor;

            if (Lightsettings.ReflectionIntensity < 0)
                Lightsettings.ReflectionIntensity = RenderSettings.reflectionIntensity;
            else
                RenderSettings.reflectionIntensity = Lightsettings.ReflectionIntensity;
            Light[] lights;
            lights = UnityEngine.Object.FindObjectsOfType<Light>();
            foreach (Light light in lights)
            {
                if (light.name == "DirectionalFront" || light.name == "Directional Front")
                {
                    if (Lightsettings.FrontLightIntensity < 0)
                        Lightsettings.FrontLightIntensity = light.intensity;
                    else
                        light.intensity = Lightsettings.FrontLightIntensity;
                    if (Lightsettings.FrontLightColor == Color.clear)
                        Lightsettings.FrontLightColor = light.color;
                    else
                        light.color = Lightsettings.FrontLightColor;
                }
                else if (light.name == "DirectionalBack" || light.name == "Directional Back")
                {
                    if (Lightsettings.BackLightIntensity < 0)
                        Lightsettings.BackLightIntensity = light.intensity;
                    else
                        light.intensity = Lightsettings.BackLightIntensity;
                    if (Lightsettings.BackLightColor == Color.clear)
                        Lightsettings.BackLightColor = light.color;
                    else
                        light.color = Lightsettings.BackLightColor;
                }
                else if (light.name == "DirectionalFrontMap" && Lightsettings.FrontLightColor != Color.clear && Lightsettings.FrontLightIntensity >= 0f)
                {
                    light.intensity = Lightsettings.FrontLightIntensity;
                    light.color = Lightsettings.FrontLightColor;
                }
                else if (light.name == "Directional Key")
                {
                    if (Lightsettings.KeyLightIntensity < 0)
                        Lightsettings.KeyLightIntensity = light.intensity;
                    else
                        light.intensity = Lightsettings.KeyLightIntensity;
                    if (Lightsettings.KeyLightColor == Color.clear)
                        Lightsettings.KeyLightColor = light.color;
                    else
                        light.color = Lightsettings.KeyLightColor;
                }
                else if (light.name == "map_hs_00_ADVLight(Clone)")
                {
                    if (Lightsettings.ADVLightIntensity < 0)
                        Lightsettings.ADVLightIntensity = light.intensity;
                    else
                        light.intensity = Lightsettings.ADVLightIntensity;
                    if (Lightsettings.ADVLightColor == Color.clear)
                        Lightsettings.ADVLightColor = light.color;
                    else
                        light.color = Lightsettings.ADVLightColor;
                }
                else if (light.name == "map_hs_00_SelectLight")
                {
                    if (Lightsettings.SelectLightIntensity < 0)
                        Lightsettings.SelectLightIntensity = light.intensity;
                    else
                        light.intensity = Lightsettings.SelectLightIntensity;
                    if (Lightsettings.SelectLightColor == Color.clear)
                        Lightsettings.SelectLightColor = light.color;
                    else
                        light.color = Lightsettings.SelectLightColor;
                }

            }
        }
    }

}
