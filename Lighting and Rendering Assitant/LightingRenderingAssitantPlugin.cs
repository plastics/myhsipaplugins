﻿using IllusionPlugin;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Reflection;
using Utility.Xml;

namespace LightingRenderingAssitant
{
    public class LightingRenderingAssitantPlugin : IEnhancedPlugin
    {
        public string Name => GetType().Name;
        public string Version => Assembly.GetExecutingAssembly().GetName().Version.ToString();

        public string[] Filter => new string[]
        {
            "StudioNEO_32",
            "StudioNEO_64",
            "HoneySelect_32",
            "HoneySelect_64"
        };

        public void OnLevelWasLoaded(int level)
        {
            //Console.WriteLine("Level " + level.ToString());
        }

        public static LightingSettingKey TitleLightings { get; set; }
        public static LightingSettingKey MyRoomLightings { get; set; }
        public static LightingSettingKey ADVsceneLightings { get; set; }
        public static LobbyLight LobbyLightings { get; set; }
        public static LightingSetting CharaMakerLightings { get; set; }
        public static LightingSetting HsceneLightings { get; set; }
        public static LightingSetting StudioNeoLightings { get; set; }

        private Control xmlLighting;

        public void OnUpdate() { }
        public void OnLateUpdate() { }

        public void OnApplicationStart()
        {
            TitleLightings = new LightingSettingKey("Title");
            MyRoomLightings = new LightingSettingKey("MyRoom");
            ADVsceneLightings = new LightingSettingKey("ADVscene");
            LobbyLightings = new LobbyLight("Lobby");
            CharaMakerLightings = new LightingSetting("CharaMaker");
            HsceneLightings = new LightingSetting("Hscene");
            StudioNeoLightings = new LightingSetting("StudioNeo");

            xmlLighting = new Control("GraphicSetting", "LightingRenderingAssist.xml", "Config", new List<Data>
            {
                TitleLightings,
                MyRoomLightings,
                ADVsceneLightings,
                LobbyLightings,
                CharaMakerLightings,
                HsceneLightings,
                StudioNeoLightings
            });

            xmlLighting.Read();
            //UnityEngine.Object.DontDestroyOnLoad(new GameObject("LightingRenderingAssitant").AddComponent<LightingRenderingAssitant>());
        }


        public void OnApplicationQuit()
        {
            xmlLighting.Write();
        }



        public void OnLevelWasInitialized(int level)
        {
            //Console.WriteLine("Lighting Rendering Assitant Plugin report: productName is "+Application.productName);
            if (Application.productName == "HoneySelect")
            {
                if(level == 3)
                {
                    LightingRenderingUtilities.SetLightingKey(TitleLightings);
                    Console.WriteLine("=========== Lighting Rendering Assitant Plugin report in Title scene ===========");
                    LightingRenderingUtilities.GatherLightInfo();
                }
                if (level == 7)
                {
                    LightingRenderingUtilities.SetLightingKey(MyRoomLightings);
                    Console.WriteLine("=========== Lighting Rendering Assitant Plugin report in My room ===========");
                    LightingRenderingUtilities.GatherLightInfo();
                }
                if (level == 12)
                {
                    LightingRenderingUtilities.SetLightingLobby(LobbyLightings);
                    Console.WriteLine("=========== Lighting Rendering Assitant Plugin report in Lobby ===========");
                    LightingRenderingUtilities.GatherLightInfo();
                }
                if (level == 15)
                {
                    LightingRenderingUtilities.SetLighting(HsceneLightings);
                    Console.WriteLine("=========== Lighting Rendering Assitant Plugin report in H scene ===========");
                    LightingRenderingUtilities.GatherLightInfo();
                }
                if (level == 17)
                {
                    LightingRenderingUtilities.SetLightingKey(ADVsceneLightings);
                    Console.WriteLine("=========== Lighting Rendering Assitant Plugin report in ADV scene ===========");
                    LightingRenderingUtilities.GatherLightInfo();
                }
                if (level == 21 || level == 22)
                {
                    LightingRenderingUtilities.SetLighting(CharaMakerLightings);
                    Console.WriteLine("=========== Lighting Rendering Assitant Plugin report in Chara Maker ===========");
                    LightingRenderingUtilities.GatherLightInfo();
                }
            }

            else if(Application.productName == "StudioNEO")
            {
                if (level == 3)
                {
                    LightingRenderingUtilities.SetLighting(StudioNeoLightings);
                    Console.WriteLine("=========== Lighting Rendering Assitant Plugin report in StudioNeo ===========");
                    LightingRenderingUtilities.GatherLightInfo();
                }
            }

        }
        public void OnFixedUpdate() { }
    }

}
