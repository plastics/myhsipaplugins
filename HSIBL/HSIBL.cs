﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;
using UnityStandardAssets.CinematicEffects;
using UnityEngine.SceneManagement;
using _4KManager;
using Manager;
using System;
using Studio;
using IllusionPlugin;

namespace HSIBL
{

    public class HSIBL : MonoBehaviour
    {

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.F5))
            {
                MainWindow = !MainWindow;
            }

            if (MainWindow)
            {
                if(Input.GetKeyDown(KeyCode.Escape))
                {
                    MainWindow = false;
                }
                if (selectedCubeMap == 0)
                {
                    if (previousambientintensity == AmbientIntensity && ((object)tempproceduralskyboxparams).Equals((object)ProceduralSkybox.skyboxparams))
                    {
                        return;
                    }
                    ProceduralSkybox.ApplySkyboxParams();
                    RenderSettings.ambientIntensity = AmbientIntensity;
                    EnvironmentUpdateFlag = true;
                    tempproceduralskyboxparams = ProceduralSkybox.skyboxparams;
                    previousambientintensity = AmbientIntensity;
                }
                else if (selectedCubeMap > 0)
                {
                    if (previousambientintensity == AmbientIntensity && ((object)tempskyboxparams).Equals((object)Skybox.skyboxparams))
                    {
                        return;
                    }
                    Skybox.ApplySkyboxParams();
                    RenderSettings.ambientIntensity = AmbientIntensity;
                    EnvironmentUpdateFlag = true;
                    tempskyboxparams = Skybox.skyboxparams;
                    previousambientintensity = AmbientIntensity;

                }
            }
        }
        private void Awake()
        {
            if (!Camera.main.hdr)
            {
                errorcode |= 1;
                Camera.main.hdr = true;
            }
            if(!GameObject.Find("4KManager"))
            {
                errorcode |= 16;
            }
            if (errorcode > 0)
            {
                Console.WriteLine("HSIBL is Loaded with error code: " + errorcode.ToString());
            }
            else
            {
                Console.WriteLine("HSIBL is Loaded");
            }
            Console.WriteLine("----------------");


            
            LightsObj = GameObject.Find("Lights");

            probeComponent = probeGameObject.AddComponent<ReflectionProbe>() as ReflectionProbe;
            probeComponent.mode = UnityEngine.Rendering.ReflectionProbeMode.Realtime;
            probeComponent.resolution = 512;
            probeComponent.hdr = true;
            probeComponent.intensity = 1f;
            probeComponent.type = UnityEngine.Rendering.ReflectionProbeType.Cube;
            probeComponent.clearFlags = UnityEngine.Rendering.ReflectionProbeClearFlags.Skybox;
            probeComponent.size = new Vector3(1000, 1000, 1000);
            probeGameObject.transform.position = new Vector3(0, 2, 0);
            probeComponent.refreshMode = UnityEngine.Rendering.ReflectionProbeRefreshMode.ViaScripting;
            probeComponent.timeSlicingMode = UnityEngine.Rendering.ReflectionProbeTimeSlicingMode.AllFacesAtOnce;


        }
        private void OnEnable()
        {

            if (!ProceduralSkybox.Proceduralsky)
            {
                ProceduralSkybox.Init();
            }
            tempproceduralskyboxparams = ProceduralSkybox.skyboxparams;
            StopAllCoroutines();
            StartCoroutine(UpdateEnvironment());
            if (Application.productName == "StudioNEO")
            {
                SubCamera = GameObject.Find("Camera").GetComponent<Camera>();
                SubCamera.fieldOfView = Camera.main.fieldOfView;
                UIUtils.windowRect = new Rect(Screen.width * 0.7f, Screen.height * 0.65f, Screen.width * 0.34f, Screen.height * 0.45f);
            }
            else if(SceneManager.GetActiveScene().buildIndex == 21 || SceneManager.GetActiveScene().buildIndex == 22)
            {

                UIUtils.windowRect = new Rect(Screen.width * 0.22f, Screen.height * 0.64f, Screen.width * 0.34f, Screen.height * 0.45f);                
                CMfemale = Singleton<Character>.Instance.GetFemale(0);
                if (CMfemale)
                {
                    RotateValue = CMfemale.GetRotation();
                    StartCoroutine(RotateCharater());
                }
            }
            else
            {
                UIUtils.windowRect = new Rect(0f, Screen.height * 0.3f, Screen.width * 0.35f, Screen.height * 0.45f);
            }
    
            BackDirectionalLight = GameObject.Find("DirectionalBack").GetComponent<Light>();
            FrontDirectionalLight = GameObject.Find("DirectionalFront").GetComponent<Light>();
            GameObject MapLight;
            if (MapLight = GameObject.Find("DirectionalFrontMap"))
                FrontDirectionalMapLight = MapLight.GetComponent<Light>();

            cubemapFolder = new FolderAssist();
            cubemapFolder.CreateFolderInfo(Application.dataPath + "/../abdata/plastic/cubemaps/", "*.unity3d", true, true);
            selectedCubeMap = -1;
            preveiousSelectedCubeMap = -1;
            CubeMapFileNames = new string[cubemapFolder.lstFile.Count+1];
            CubeMapFileNames[0] = "Procedural";
            int count = 1;
            foreach (FolderAssist.FileInfo fileInfo in cubemapFolder.lstFile)
            {
                CubeMapFileNames[count] = fileInfo.FileName;
                count++;
            }

            ToneMappingManager = Camera.main.GetComponent<ColorCorrectionCurves>().Tonemapping;
            BloomManager = Camera.main.GetComponent<ColorCorrectionCurves>().CinematicBloom;
            LensManager = Camera.main.GetComponent<ColorCorrectionCurves>().LensAberrations;

        }

        private IEnumerator RotateCharater()
        {
            while(true)
            {
                if (autoRotate && CMfemale != null)
                {
                    CharaRotate += autoRotateSpeed;
                    if (CharaRotate > 180f)
                    {
                        CharaRotate -= 360f;
                    }
                    else if (CharaRotate < -180f)
                    {
                        CharaRotate += 360f;
                    }
                    CMfemale.SetRotation(RotateValue.x, RotateValue.y + CharaRotate, RotateValue.z);
                }
                yield return new WaitForEndOfFrame();
            }            
        }

        public void CameraControlOffOnGUI()
        {
            switch (Event.current.type)
            {
                case EventType.MouseDown:
                case EventType.MouseDrag:
                    cameraCtrlOff = true;
                    break;
                default:
                    return;       
            }
            
        }

        private void OnGUI()
        {
            if (!MainWindow)
            {
                return;
            }
            if (!UIUtils.styleInitialized)
            {
                UIUtils.InitStyle();
            }
            if (!Camera.main.hdr)
            {
                
                if(Camera.main.actualRenderingPath != RenderingPath.DeferredShading)
                {
                    UIUtils.CMWarningRect = GUILayout.Window(CMWaringWindowID, UIUtils.CMWarningRect, CharaMakerWarningWindow, "Warning", UIUtils.windowstyle);
                    return;
                }
                Console.WriteLine("HSIBL Warning: HDR is somehow been disabled! Trying to re-enable it...");
                Camera.main.hdr = true; 
                if(!Camera.main.hdr)
                {
                    Console.WriteLine("HSIBL Error: Failed to enable HDR");
                    MainWindow = false;
                    return;
                }
                Console.WriteLine("HSIBL Info: Done!");
            }

            GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, UIUtils.scale);



            if (errorcode > 1)
            {
                UIUtils.ErrorwindowRect = GUILayout.Window(ErrorwindowID, UIUtils.ErrorwindowRect, ErrorWindow, "", UIUtils.windowstyle);
                return;
            }
            if (Event.current.type == EventType.MouseDown || Event.current.type == EventType.MouseUp)
            {
                cameraCtrlOff = false;
                windowdragflag = false;
            }
            UIUtils.windowRect = UIUtils.LimitWindowRect(UIUtils.windowRect);
            UIUtils.windowRect = GUILayout.Window(windowID, UIUtils.windowRect, HSIBLWindow, "", UIUtils.windowstyle);
            GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Vector3.one);

        }

        private void CubeMapModule()
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(" Load Cubemaps: ", UIUtils.labelstyle);
            GUILayout.FlexibleSpace();
            hideSkybox = UIUtils.ToggleButton(hideSkybox, new GUIContent("Hide", "Hide skybox in the background"));
            GUILayout.EndHorizontal();
            if (hideSkybox)
            {
                Camera.main.clearFlags = CameraClearFlags.SolidColor;
            }
            else if (cubemaploaded)
            {
                Camera.main.clearFlags = CameraClearFlags.Skybox;
            }
            UIUtils.scrollPosition[0] = GUILayout.BeginScrollView(UIUtils.scrollPosition[0]);

            selectedCubeMap = GUILayout.SelectionGrid(selectedCubeMap, CubeMapFileNames, 1, UIUtils.buttonstyleStrechWidth);
            
            if(selectedCubeMap > 0 && preveiousSelectedCubeMap !=  selectedCubeMap)
            {
                StartCoroutine(Loadcubemap(cubemapFolder.lstFile[selectedCubeMap-1].FileName));
                preveiousSelectedCubeMap = selectedCubeMap;
            }
            if(selectedCubeMap == 0 && preveiousSelectedCubeMap != 0)
            {

                ProceduralSkybox.ApplySkybox();
                ProceduralSkybox.ApplySkyboxParams();
                EnvironmentUpdateFlag = true;
                preveiousSelectedCubeMap = 0;
                cubemaploaded = true;
            }

            GUILayout.EndScrollView();
        }
        

        private void CharaRotateModule()
        {
            if (CMfemale)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label("Rotate Character", UIUtils.titlestyle2);
                GUILayout.FlexibleSpace();
                autoRotate = UIUtils.ToggleButton(autoRotate, new GUIContent("Auto Rotate"));
                GUILayout.EndHorizontal();
                if (autoRotate)
                {
                    GUILayout.Label(" Auto Rotate Speed " + autoRotateSpeed.ToString("N3"), UIUtils.labelstyle);
                    autoRotateSpeed = GUILayout.HorizontalSlider(autoRotateSpeed, -5f, 5f, UIUtils.sliderstyle, UIUtils.thumbstyle);
                }
                else
                {
                    CharaRotate = GUILayout.HorizontalSlider(CharaRotate, -180f, 180f, UIUtils.sliderstyle, UIUtils.thumbstyle);
                    CMfemale.SetRotation(RotateValue.x, RotateValue.y + CharaRotate, RotateValue.z);
                }
                GUILayout.Space(UIUtils.space);
            }
        }
        private void UserCustomModule()
        {

            GUILayout.BeginHorizontal();
            GUILayout.Label(GUIStrings.Custom_Window, UIUtils.labelstyle);
            GUILayout.FlexibleSpace();
            if (GUILayout.Button(GUIStrings.Custom_Window_Remember, UIUtils.buttonstyleNoStretch, GUILayout.ExpandWidth(false)))
            {
                ModPrefs.SetFloat("HSIBL", "Window.width", UIUtils.windowRect.width);
                ModPrefs.SetFloat("HSIBL", "Window.height", UIUtils.windowRect.height);
                ModPrefs.SetFloat("HSIBL", "Window.x", UIUtils.windowRect.x);
                ModPrefs.SetFloat("HSIBL", "Window.y", UIUtils.windowRect.y);
            }
            GUILayout.EndHorizontal();
            GUILayout.Space(UIUtils.space);
            float widthtemp;
            widthtemp = UIUtils.SliderGUI(
                value: UIUtils.windowRect.width,
                min: UIUtils.minwidth,
                max: UIUtils.Screen.width * 0.5f,
                reset: GetWindowWidth,
                labeltext: GUIStrings.Window_Width,
                valuedecimals: "N0");
            if (widthtemp != UIUtils.windowRect.width)
            {
                UIUtils.windowRect.x += (UIUtils.windowRect.width - widthtemp) * (UIUtils.windowRect.x) / (UIUtils.Screen.width - UIUtils.windowRect.width);
                UIUtils.windowRect.width = widthtemp;
            }
            UIUtils.windowRect.height = UIUtils.SliderGUI(
                value: UIUtils.windowRect.height,
                min: UIUtils.Screen.height * 0.2f,
                max: UIUtils.Screen.height - 10f,
                reset: GetWindowHeight,
                labeltext: GUIStrings.Window_Height,
                valuedecimals: "N0");
        }
        Func<float> GetWindowHeight = () => ModPrefs.GetFloat("HSIBL", "Window.height");
        Func<float> GetWindowWidth = () => ModPrefs.GetFloat("HSIBL", "Window.width");
        private void CharaMakerWarningWindow(int id)
        {
            GUILayout.BeginVertical();
            GUILayout.Label(" Using image based lighting in the chara maker requires deferred renderingpath and HDR, meanwhile you can't use background image. DO you want to continue? ", UIUtils.labelstyle3);
            GUILayout.FlexibleSpace();
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button(" Just this time ", UIUtils.buttonstyleNoStretch))
            {
                Console.WriteLine("HSIBL Info: Changing rendering path to deferred shading.");
                Camera.main.renderingPath = RenderingPath.DeferredShading;
                Camera.main.clearFlags = CameraClearFlags.SolidColor;
                //return;
            }
            GUILayout.FlexibleSpace();
            if (GUILayout.Button(" Always ", UIUtils.buttonstyleNoStretch))
            {
                GraphicSetting.BasicSettings.CharaMakerReform = true;
                Console.WriteLine("HSIBL Info: Changing rendering path to deferred shading.");
                Camera.main.renderingPath = RenderingPath.DeferredShading;
                Camera.main.clearFlags = CameraClearFlags.SolidColor;
                //return;
            }
            GUILayout.FlexibleSpace();
            if (GUILayout.Button(" No ", UIUtils.buttonstyleNoStretch))
            {
                MainWindow = false;
                //return;
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();

        }

        private void ErrorWindow(int id)
        {

            GUILayout.BeginVertical();
            GUILayout.Label(" Error " + errorcode.ToString() + ": Please make sure you have installed HS linear rendering experiment (Version ≥ 3). ", UIUtils.labelstyle3);
            GUILayout.FlexibleSpace();
            if (GUILayout.Button(" OK ", UIUtils.buttonstyleNoStretch))
            {
                MainWindow = false;
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
        }

        bool windowdragflag = false;


        private void HSIBLWindow(int id)
        {

            //CameraControlOffOnGUI();
            if (Event.current.type == EventType.MouseDown)
            {
                GUI.FocusWindow(windowID);
                windowdragflag = true;
                cameraCtrlOff = true;

            }
            else if (Event.current.type == EventType.MouseUp)
            {
                windowdragflag = false;
                cameraCtrlOff = false;
            }
            if (windowdragflag && Event.current.type == EventType.MouseDrag)
            {
                cameraCtrlOff = true;
            }

            GUILayout.BeginHorizontal();
            using (var verticalScope = new GUILayout.VerticalScope("box", GUILayout.MaxWidth(UIUtils.windowRect.width * 0.33f)))
            {
                //////////////////Load cubemaps/////////////// 
                GUI.enabled = !IsLoading;
                CubeMapModule();
                GUI.enabled = true;

            }
            GUILayout.Space(1f);
            GUILayout.BeginVertical();
            TabMenu = GUILayout.Toolbar(TabMenu, GUIStrings.Titlebar, UIUtils.titlestyle);
            UIUtils.scrollPosition[TabMenu+1] = GUILayout.BeginScrollView(UIUtils.scrollPosition[TabMenu + 1]);
            using (var verticalScope = new GUILayout.VerticalScope("box", GUILayout.MaxHeight(Screen.height * 0.8f)))
            {
                
                GUILayout.Space(UIUtils.space);
                if (TabMenu == 0)
                {
                    ////////////////////Lighting tweak/////////////////////
                    
                    if (selectedCubeMap == 0)
                        ProceduralSkyboxModule();
                    else 
                        SkyboxModule();
                    GUILayout.Space(UIUtils.space);
                    ReflectionModule();
                    GUILayout.Space(UIUtils.space);
                    DefaultLightModule();

                }

                else if (TabMenu == 1)
                {
                    LensPresetsModule();
                    LensModule();
                }
                else if (TabMenu == 2)
                {
                    ///////////////////////Tone mapping///////////////////// 
                    ToneMappingModule();
                    //////////////////////eye adaptation/////////////////////
                    EyeAdaptationModule();
                    //////////////////////Bloom//////////////////////////
                    BloomModule();
                }
                else if(TabMenu == 3)
                {
                    CharaRotateModule();
                    UserCustomModule();
                }
            }
            GUILayout.EndScrollView();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button(" Auto Mode ", UIUtils.buttonstyleNoStretch))
            {
                OptimalSetting(true);
            }

            if (GUILayout.Button(" Manual Mode ", UIUtils.buttonstyleNoStretch))
            {
                OptimalSetting(false);
            }
            GUILayout.EndHorizontal();
            GUI.DragWindow();
        }

        private void ProceduralSkyboxModule()
        {
            GUILayout.Label("Procedural Skybox", UIUtils.titlestyle2);
            GUILayout.Space(UIUtils.space);
            ProceduralSkybox.skyboxparams.exposure = UIUtils.SliderGUI(ProceduralSkybox.skyboxparams.exposure, 0f, 8f, 1f, " Skybox Exposure: ", "", "N3");
            ProceduralSkybox.skyboxparams.sunsize = UIUtils.SliderGUI(ProceduralSkybox.skyboxparams.sunsize, 0f, 1f, 0.1f, " Sun Size : ", "", "N3");
            ProceduralSkybox.skyboxparams.atmospherethickness = UIUtils.SliderGUI(ProceduralSkybox.skyboxparams.atmospherethickness, 0f, 5f, 1f, " Atmosphere Thickness: ", "", "N3");
            ProceduralSkybox.skyboxparams.skytint = UIUtils.SliderGUI(ProceduralSkybox.skyboxparams.skytint, Color.gray, " Sky Tint: ", "");
            ProceduralSkybox.skyboxparams.groundcolor = UIUtils.SliderGUI(ProceduralSkybox.skyboxparams.groundcolor, Color.gray, " Gound Color: ", "");
            AmbientIntensity = UIUtils.SliderGUI(AmbientIntensity, 0f, 2f, 1f, " Ambient Intensity: ", "", "N3");
        }

        private void BloomModule()
        {
            GUILayout.Label(GUIStrings.Bloom, UIUtils.titlestyle2);
            GUILayout.Space(UIUtils.space);
            GUILayout.Label("Bloom Intensity: " + BloomManager.settings.intensity.ToString("N3"), UIUtils.labelstyle);
            BloomManager.settings.intensity = GUILayout.HorizontalSlider(BloomManager.settings.intensity, 0f, 1f, UIUtils.sliderstyle, UIUtils.thumbstyle);
            GUILayout.Label("Bloom Threshold: " + BloomManager.settings.threshold.ToString("N3"), UIUtils.labelstyle);
            BloomManager.settings.threshold = GUILayout.HorizontalSlider(BloomManager.settings.threshold, 0f, 8f, UIUtils.sliderstyle, UIUtils.thumbstyle);
            GUILayout.Label("Bloom Softknee: " + BloomManager.settings.softKnee.ToString("N3"), UIUtils.labelstyle);
            BloomManager.settings.softKnee = GUILayout.HorizontalSlider(BloomManager.settings.softKnee, 0f, 1f, UIUtils.sliderstyle, UIUtils.thumbstyle);
            GUILayout.Label("Bloom Radius: " + BloomManager.settings.radius.ToString("N3"), UIUtils.labelstyle);
            BloomManager.settings.radius = GUILayout.HorizontalSlider(BloomManager.settings.radius, 1f, 7f, UIUtils.sliderstyle, UIUtils.thumbstyle);
            BloomManager.settings.antiFlicker = UIUtils.ToggleGUI(BloomManager.settings.antiFlicker, GUIStrings.Bloom_antiflicker,GUIStrings.Disable_vs_Enable);
        }
        private void EyeAdaptationModule()
        {
            eye_enabled = UIUtils.ToggleGUI(ToneMappingManager.eyeAdaptation.enabled, GUIStrings.Eye_Adaptation, GUIStrings.Disable_vs_Enable);
            eye_middleGrey = UIUtils.SliderGUI(ToneMappingManager.eyeAdaptation.middleGrey, 0f, 0.5f, 0.1f, "Middle Grey: ","N3");
            eye_min = UIUtils.SliderGUI(ToneMappingManager.eyeAdaptation.min, -8f, 0f, -4f, "lowest exposure value: ", "N3");
            eye_max = UIUtils.SliderGUI(ToneMappingManager.eyeAdaptation.max, 0f, 8f, 4f, "highest exposure value : ", "N3");
            eye_speed = UIUtils.SliderGUI(ToneMappingManager.eyeAdaptation.speed, 0f, 8f, "Adaptation Speed: ","N3");
            ToneMappingManager.eyeAdaptation = new TonemappingColorGrading.EyeAdaptationSettings
            {
                enabled = eye_enabled,
                showDebug = false,
                middleGrey = eye_middleGrey,
                max = eye_max,
                min = eye_min,
                speed = eye_speed
            };
        }
        private void LensPresetsModule()
        {
            GUILayout.Label("Lens Presets", UIUtils.labelstyle);
            GUILayout.Space(UIUtils.space);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("85mm", UIUtils.buttonstyleStrechWidth))
            {
                Camera.main.fieldOfView = 23.9f;
                if (SubCamera != null)
                {
                    SubCamera.fieldOfView = Camera.main.fieldOfView;
                }
                LensManager.distortion.enabled = false;
                LensManager.vignette.enabled = true;
                LensManager.vignette.intensity = 0.7f;
                LensManager.vignette.color = Color.black;
                LensManager.vignette.blur = 0f;
                LensManager.vignette.desaturate = 0f;
                LensManager.vignette.roundness = 0.5625f;
                LensManager.vignette.smoothness = 2f;
            }
            if (GUILayout.Button("50mm", UIUtils.buttonstyleStrechWidth))
            {
                Camera.main.fieldOfView = 39.6f;
                if (SubCamera != null)
                {
                    SubCamera.fieldOfView = Camera.main.fieldOfView;
                }
                LensManager.distortion.enabled = false;
                LensManager.vignette.enabled = true;
                LensManager.vignette.intensity = 1f;
                LensManager.vignette.color = Color.black;
                LensManager.vignette.blur = 0f;
                LensManager.vignette.desaturate = 0f;
                LensManager.vignette.roundness = 0.5625f;
                LensManager.vignette.smoothness = 2f;
            }
            if (GUILayout.Button("35mm", UIUtils.buttonstyleStrechWidth))
            {
                Camera.main.fieldOfView = 57.9f;
                if (SubCamera != null)
                {
                    SubCamera.fieldOfView = Camera.main.fieldOfView;
                }
                LensManager.distortion.enabled = false;
                LensManager.vignette.enabled = true;
                LensManager.vignette.intensity = 1.6f;
                LensManager.vignette.color = Color.black;
                LensManager.vignette.blur = 0f;
                LensManager.vignette.desaturate = 0f;
                LensManager.vignette.roundness = 0.5625f;
                LensManager.vignette.smoothness = 1.6f;
            }
            if (GUILayout.Button("24mm", UIUtils.buttonstyleStrechWidth))
            {
                Camera.main.fieldOfView = 85.5f;
                if (SubCamera != null)
                {
                    SubCamera.fieldOfView = Camera.main.fieldOfView;
                }
                LensManager.distortion.enabled = true;
                LensManager.distortion.amount = 25f;
                LensManager.distortion.amountX = 1f;
                LensManager.distortion.amountY = 1f;
                LensManager.distortion.scale = 1.025f;
                LensManager.vignette.enabled = true;
                LensManager.vignette.intensity = 1.8f;
                LensManager.vignette.color = Color.black;
                LensManager.vignette.blur = 0.1f;
                LensManager.vignette.desaturate = 0f;
                LensManager.vignette.roundness = 0.187f;
                LensManager.vignette.smoothness = 1.4f;
            }
            if (GUILayout.Button("16mm", UIUtils.buttonstyleStrechWidth))
            {
                Camera.main.fieldOfView = 132.6f;
                if (SubCamera != null)
                {
                    SubCamera.fieldOfView = Camera.main.fieldOfView;
                }
                LensManager.distortion.enabled = true;
                LensManager.distortion.amount = 69f;
                LensManager.distortion.amountX = 1f;
                LensManager.distortion.amountY = 1f;
                LensManager.distortion.scale = 1.05f;
                LensManager.vignette.enabled = true;
                LensManager.vignette.intensity = 1.95f;
                LensManager.vignette.color = Color.black;
                LensManager.vignette.blur = 0.14f;
                LensManager.vignette.desaturate = 0.14f;
                LensManager.vignette.roundness = 0.814f;
                LensManager.vignette.smoothness = 1.143f;
            }
            GUILayout.EndHorizontal();
        }
        private void LensModule()
        {
            //////////////////////Field of View/////////////////////
            GUILayout.Label("Field of View: " + Camera.main.fieldOfView.ToString("N1"), UIUtils.labelstyle);
            Camera.main.fieldOfView = GUILayout.HorizontalSlider(Camera.main.fieldOfView, 10f, 150f, UIUtils.sliderstyle, UIUtils.thumbstyle);
            if (SubCamera != null)
            {
                SubCamera.fieldOfView = Camera.main.fieldOfView;
            }
            GUILayout.Space(UIUtils.space);
            //////////////////////Lens Aberration/////////////////
            LensManager.distortion.enabled = UIUtils.ToggleGUI(LensManager.distortion.enabled, new GUIContent("Distortion"), GUIStrings.Disable_vs_Enable);
            if (LensManager.distortion.enabled)
            {
                GUILayout.Label("Distortion Amount: " + LensManager.distortion.amount.ToString("N3"), UIUtils.labelstyle);
                LensManager.distortion.amount = GUILayout.HorizontalSlider(LensManager.distortion.amount, -100f, 100f, UIUtils.sliderstyle, UIUtils.thumbstyle);
                GUILayout.Label("Amount multiplier on X axis: " + LensManager.distortion.amountX.ToString("N3"), UIUtils.labelstyle);
                LensManager.distortion.amountX = GUILayout.HorizontalSlider(LensManager.distortion.amountX, 0f, 1f, UIUtils.sliderstyle, UIUtils.thumbstyle);
                GUILayout.Label("Amount multiplier on Y axis: " + LensManager.distortion.amountY.ToString("N3"), UIUtils.labelstyle);
                LensManager.distortion.amountY = GUILayout.HorizontalSlider(LensManager.distortion.amountY, 0f, 1f, UIUtils.sliderstyle, UIUtils.thumbstyle);
                GUILayout.Label("Global screen scale: " + LensManager.distortion.scale.ToString("N3"), UIUtils.labelstyle);
                LensManager.distortion.scale = GUILayout.HorizontalSlider(LensManager.distortion.scale, 0.5f, 2f, UIUtils.sliderstyle, UIUtils.thumbstyle);
            }
            GUILayout.Space(UIUtils.space);

            LensManager.chromaticAberration.enabled = UIUtils.ToggleGUI(LensManager.chromaticAberration.enabled, new GUIContent(GUIStrings.Chromatic_Aberration), GUIStrings.Disable_vs_Enable);
            if(LensManager.chromaticAberration.enabled)
            {
                GUILayout.Label("Tangential distortion Amount: " + LensManager.chromaticAberration.amount.ToString("N3"), UIUtils.labelstyle);
                LensManager.chromaticAberration.amount = GUILayout.HorizontalSlider(LensManager.chromaticAberration.amount, -4f, 4f, UIUtils.sliderstyle, UIUtils.thumbstyle);
            }
            GUILayout.Space(UIUtils.space);

            LensManager.vignette.enabled = UIUtils.ToggleGUI(LensManager.vignette.enabled, new GUIContent(GUIStrings.Vignette), GUIStrings.Disable_vs_Enable);
            if (LensManager.vignette.enabled)
            {
                GUILayout.Label("Vignette Intensity: " + LensManager.vignette.intensity.ToString("N3"), UIUtils.labelstyle);
                LensManager.vignette.intensity = GUILayout.HorizontalSlider(LensManager.vignette.intensity, 0f, 3f, UIUtils.sliderstyle, UIUtils.thumbstyle);
                GUILayout.Label("Vignette Smothness: " + LensManager.vignette.smoothness.ToString("N3"), UIUtils.labelstyle);
                LensManager.vignette.smoothness = GUILayout.HorizontalSlider(LensManager.vignette.smoothness, 0.01f, 3f, UIUtils.sliderstyle, UIUtils.thumbstyle);
                GUILayout.Label("Vignette Roundness: " + LensManager.vignette.roundness.ToString("N3"), UIUtils.labelstyle);
                LensManager.vignette.roundness = GUILayout.HorizontalSlider(LensManager.vignette.roundness, 0f, 1f, UIUtils.sliderstyle, UIUtils.thumbstyle);
                GUILayout.Label("Vignette Desaturate: " + LensManager.vignette.desaturate.ToString("N3"), UIUtils.labelstyle);
                LensManager.vignette.desaturate = GUILayout.HorizontalSlider(LensManager.vignette.desaturate, 0f, 1f, UIUtils.sliderstyle, UIUtils.thumbstyle);
                GUILayout.Label("Blur Corner: " + LensManager.vignette.blur.ToString("N3"), UIUtils.labelstyle);
                LensManager.vignette.blur = GUILayout.HorizontalSlider(LensManager.vignette.blur, 0f, 1f, UIUtils.sliderstyle, UIUtils.thumbstyle);
                LensManager.vignette.color = UIUtils.SliderGUI(LensManager.vignette.color, Color.black, GUIStrings.Vignette_color);

            }
           


        }

        private void DefaultLightModule()
        {
            GUILayout.Label("Directional Light", UIUtils.titlestyle2);
            GUILayout.Space(UIUtils.space);
            GUILayout.BeginHorizontal();
            GUILayout.Label(" Front Light ", UIUtils.labelstyle);
            GUILayout.FlexibleSpace();
            FrontLightAnchor = UIUtils.ToggleButton(!(FrontDirectionalLight.transform.parent == null), new GUIContent(" Rotate with camera "));
            GUILayout.EndHorizontal();
           
            if(!FrontLightAnchor)
            {
                LightsObj.transform.parent = null;
                FrontDirectionalLight.transform.parent = null;                
                FrontRotate.x = UIUtils.SliderGUI(FrontRotate.x, 0f, 360f, 0f, "Vertical rotation","N1");                
                FrontRotate.y = UIUtils.SliderGUI(FrontRotate.y, 0f, 360f, 0f, "Horizontal rotation", "N1");
                FrontDirectionalLight.transform.eulerAngles = FrontRotate;
            }
            else
            {
                FrontRotate = FrontDirectionalLight.transform.eulerAngles;
                LightsObj.transform.parent = Camera.main.transform;
                FrontDirectionalLight.transform.parent = LightsObj.transform;
            }
            FrontDirectionalLight.intensity = UIUtils.SliderGUI(FrontDirectionalLight.intensity, 0f, 8f, 1f, " Intensity: ", "N3");
            FrontDirectionalLight.color = UIUtils.SliderGUI(FrontDirectionalLight.color, Color.white, " Color: ");

            GUILayout.BeginHorizontal();
            GUILayout.Label(" Back Light ", UIUtils.labelstyle);
            GUILayout.FlexibleSpace();
            BackLightAnchor = UIUtils.ToggleButton(!(BackDirectionalLight.transform.parent == null), new GUIContent(" Rotate with camera "));
            GUILayout.EndHorizontal();
            if (!BackLightAnchor)
            {
                BackDirectionalLight.transform.parent = null;                
                BackRotate.x = UIUtils.SliderGUI(BackRotate.x, 0f, 360f, 0f, "Vertical rotation", "", "N1");
                BackRotate.y = UIUtils.SliderGUI(BackRotate.y, 0f, 360f, 0f, "Horizontal rotation", "", "N1");
                BackDirectionalLight.transform.eulerAngles = BackRotate;
            }
            else
            {
                BackRotate = BackDirectionalLight.transform.eulerAngles;
                BackDirectionalLight.transform.parent = Camera.main.transform;
            }
            BackDirectionalLight.intensity = UIUtils.SliderGUI(BackDirectionalLight.intensity, 0f, 8f, 1f, " Intensity: ", "", "N3");
            BackDirectionalLight.color = UIUtils.SliderGUI(BackDirectionalLight.color, Color.white, GUIStrings.Color, "");
        }

        private void SkyboxModule()
        {
            GUILayout.Label("Skybox", UIUtils.titlestyle2);
            GUILayout.Space(UIUtils.space);
            Skybox.skyboxparams.rotation = UIUtils.SliderGUI(Skybox.skyboxparams.rotation, 0f, 360f, 0f, " Skybox Rotation: ", "", "N3");
            Skybox.skyboxparams.exposure = UIUtils.SliderGUI(Skybox.skyboxparams.exposure, 0f, 8f, 1f, " Skybox Exposure: ", "", "N3");
            Skybox.skyboxparams.tint = UIUtils.SliderGUI(Skybox.skyboxparams.tint, Color.gray, " Skybox Tint: ", "");
            AmbientIntensity = UIUtils.SliderGUI(AmbientIntensity, 0f, 2f, 1f, " Ambient Intensity: ", "", "N3");
        }

        private void ReflectionModule()
        {
            GUILayout.Label(GUIStrings.Reflection, UIUtils.titlestyle2);
            GUILayout.Space(UIUtils.space);

            GUILayout.BeginHorizontal();
            GUILayout.Label(GUIStrings.Reflection_probe_refresh, UIUtils.labelstyle);
            GUILayout.Space(UIUtils.space);
            if (probeComponent.refreshMode == UnityEngine.Rendering.ReflectionProbeRefreshMode.ViaScripting)
            {
                if (GUILayout.Button(GUIStrings.Reflection_probe_refresh, UIUtils.buttonstyleNoStretch))
                {
                    probeComponent.RenderProbe();
                }
            }
            GUILayout.EndHorizontal();

            if (probeComponent.refreshMode == UnityEngine.Rendering.ReflectionProbeRefreshMode.ViaScripting)
            {
                ReflectionProbeRefreshRate = 0;
            }
            else if (probeComponent.timeSlicingMode == UnityEngine.Rendering.ReflectionProbeTimeSlicingMode.AllFacesAtOnce)
            {
                ReflectionProbeRefreshRate = 2;
            }
            else if (probeComponent.timeSlicingMode == UnityEngine.Rendering.ReflectionProbeTimeSlicingMode.IndividualFaces)
            {
                ReflectionProbeRefreshRate = 1;
            }
            else if (probeComponent.timeSlicingMode == UnityEngine.Rendering.ReflectionProbeTimeSlicingMode.NoTimeSlicing)
            {
                ReflectionProbeRefreshRate = 3;
            }
            ReflectionProbeRefreshRate = GUILayout.SelectionGrid(ReflectionProbeRefreshRate, GUIStrings.Reflection_probe_refresh_rate_Array, 4, UIUtils.selectstyle);

            switch (ReflectionProbeRefreshRate)
            {
                default:
                case 0:
                    probeComponent.refreshMode = UnityEngine.Rendering.ReflectionProbeRefreshMode.ViaScripting;
                    break;
                case 1:
                    probeComponent.refreshMode = UnityEngine.Rendering.ReflectionProbeRefreshMode.EveryFrame;
                    probeComponent.timeSlicingMode = UnityEngine.Rendering.ReflectionProbeTimeSlicingMode.IndividualFaces;
                    break;
                case 2:
                    probeComponent.refreshMode = UnityEngine.Rendering.ReflectionProbeRefreshMode.EveryFrame;
                    probeComponent.timeSlicingMode = UnityEngine.Rendering.ReflectionProbeTimeSlicingMode.AllFacesAtOnce;
                    break;
                case 3:
                    probeComponent.refreshMode = UnityEngine.Rendering.ReflectionProbeRefreshMode.EveryFrame;
                    probeComponent.timeSlicingMode = UnityEngine.Rendering.ReflectionProbeTimeSlicingMode.NoTimeSlicing;
                    break;
            }

            GUILayout.Label(GUIStrings.Reflection_probe_resolution, UIUtils.labelstyle);

            switch (probeComponent.resolution)
            {
                case 128:
                    ReflectionProbeResolution = 0;
                    break;
                default:
                case 256:
                    ReflectionProbeResolution = 1;
                    break;
                case 512:
                    ReflectionProbeResolution = 2;
                    break;
            }

            ReflectionProbeResolution = GUILayout.SelectionGrid(ReflectionProbeResolution, new string[]
            {
                    "128",
                    "256",
                    "512"
            }, 3, UIUtils.selectstyle);

            switch (ReflectionProbeResolution)
            {
                case 0:
                    probeComponent.resolution = 128;
                    break;
                default:
                case 1:
                    probeComponent.resolution = 256;
                    break;
                case 2:
                    probeComponent.resolution = 512;
                    break;
            }
            if (Application.productName == "StudioNEO")
            {
                if (GUILayout.Button(" Move Reflection Probe to target ", UIUtils.buttonstyleNoStretch))
                {
                    probeGameObject.transform.position = cameraCtrl.targetTex.position;
                    if (probeComponent.refreshMode == UnityEngine.Rendering.ReflectionProbeRefreshMode.ViaScripting)
                    {
                        probeComponent.RenderProbe();
                    }
                }
            }
            GUILayout.Space(UIUtils.space);
            probeComponent.intensity = UIUtils.SliderGUI(probeComponent.intensity, 0f, 2f, 1f, GUIStrings.Reflection_Intensity, "N3");

        }

        private void ToneMappingModule()
        {
            Tonemapping_enabled = UIUtils.ToggleGUI(ToneMappingManager.tonemapping.enabled, GUIStrings.Tonemapping, GUIStrings.Disable_vs_Enable);

            if (Tonemapping_enabled)
            {
                switch (ToneMappingManager.tonemapping.tonemapper)
                {
                    default:
                    case TonemappingColorGrading.Tonemapper.ACES:
                        SelectedToneMapper = 0;
                        break;
                    case TonemappingColorGrading.Tonemapper.Hable:
                        SelectedToneMapper = 1;
                        break;
                    case TonemappingColorGrading.Tonemapper.HejlDawson:
                        SelectedToneMapper = 2;
                        break;
                    case TonemappingColorGrading.Tonemapper.Photographic:
                        SelectedToneMapper = 3;
                        break;
                    case TonemappingColorGrading.Tonemapper.Reinhard:
                        SelectedToneMapper = 4;
                        break;
                }
                GUILayout.Space(5f);
                SelectedToneMapper = GUILayout.SelectionGrid(SelectedToneMapper, new string[]
                {
                    "ACES",
                    "Hable",
                    "HejlDawson",
                    "Photographic",
                    "Reinhard"
                }, 3, UIUtils.selectstyle);

                switch (SelectedToneMapper)
                {
                    default:
                    case 0:
                        ToneMapper = TonemappingColorGrading.Tonemapper.ACES;
                        break;
                    case 1:
                        ToneMapper = TonemappingColorGrading.Tonemapper.Hable;
                        break;
                    case 2:
                        ToneMapper = TonemappingColorGrading.Tonemapper.HejlDawson;
                        break;
                    case 3:
                        ToneMapper = TonemappingColorGrading.Tonemapper.Photographic;
                        break;
                    case 4:
                        ToneMapper = TonemappingColorGrading.Tonemapper.Reinhard;
                        break;
                }
                EV = UIUtils.SliderGUI(EV, -5f, 5f, 0f, new GUIContent(GUIStrings.Exposure_Value), "N3");
            }
            ToneMappingManager.tonemapping = new TonemappingColorGrading.TonemappingSettings
            {
                tonemapper = ToneMapper,
                exposure = Mathf.Pow(2f, EV),
                enabled = Tonemapping_enabled,
                neutralBlackIn = ToneMappingManager.tonemapping.neutralBlackIn,
                neutralBlackOut = ToneMappingManager.tonemapping.neutralBlackOut,
                neutralWhiteClip = ToneMappingManager.tonemapping.neutralWhiteClip,
                neutralWhiteIn = ToneMappingManager.tonemapping.neutralWhiteIn,
                neutralWhiteLevel = ToneMappingManager.tonemapping.neutralWhiteLevel,
                neutralWhiteOut = ToneMappingManager.tonemapping.neutralWhiteOut,
                curve = ToneMappingManager.tonemapping.curve
            };
        }

        private void OptimalSetting(bool auto)
        {
            if (Application.productName == "StudioNEO")
            {
                cameraCtrl = Singleton<Studio.Studio>.Instance.cameraCtrl;
                SceneInfo sceneInfo = Singleton<Studio.Studio>.Instance.sceneInfo;
                sceneInfo.enableFog = false;
                sceneInfo.enableDepth = false;
                sceneInfo.enableSunShafts = false;
                SystemButtonCtrl sysbctrl = Singleton<Studio.Studio>.Instance.systemButtonCtrl;
                sysbctrl.UpdateInfo();
            }
            BloomManager.settings.intensity = 0.1f;
            BloomManager.settings.radius = 5f;
            BloomManager.settings.threshold = 5f;
            BloomManager.settings.softKnee = 0.5f;
            FrontDirectionalLight.intensity = 1f;
            BackDirectionalLight.intensity = 0f;
            AmbientIntensity = 1f;
            RenderSettings.reflectionIntensity = 1f;
            probeComponent.intensity = 1f;
            ToneMappingManager.eyeAdaptation = new TonemappingColorGrading.EyeAdaptationSettings
            {
                enabled = auto,
                showDebug = false,
                middleGrey = 0.09f,
                max = 4f,
                min = -4f,
                speed = 2f
            };
            ToneMappingManager.tonemapping = new TonemappingColorGrading.TonemappingSettings
            {
                tonemapper = TonemappingColorGrading.Tonemapper.ACES,
                exposure = 1f,
                enabled = true,
                neutralBlackIn = ToneMappingManager.tonemapping.neutralBlackIn,
                neutralBlackOut = ToneMappingManager.tonemapping.neutralBlackOut,
                neutralWhiteClip = ToneMappingManager.tonemapping.neutralWhiteClip,
                neutralWhiteIn = ToneMappingManager.tonemapping.neutralWhiteIn,
                neutralWhiteLevel = ToneMappingManager.tonemapping.neutralWhiteLevel,
                neutralWhiteOut = ToneMappingManager.tonemapping.neutralWhiteOut,
                curve = ToneMappingManager.tonemapping.curve
            };
        }


        private IEnumerator UpdateEnvironment()
        {
            while (true)
            {
                if (EnvironmentUpdateFlag)
                {
                    DynamicGI.UpdateEnvironment();
                    if (probeComponent.refreshMode == UnityEngine.Rendering.ReflectionProbeRefreshMode.ViaScripting)
                    {
                        probeComponent.RenderProbe();
                    }
                    EnvironmentUpdateFlag = false;
                }
                yield return new WaitForEndOfFrame();
            }
        }

        private IEnumerator Loadcubemap(string filename)
        {
            IsLoading = true;
            AssetBundleCreateRequest assetBundleCreateRequest = AssetBundle.LoadFromFileAsync(Application.dataPath + "/../abdata/plastic/cubemaps/" + filename + ".unity3d");
            yield return assetBundleCreateRequest;
            AssetBundle cubemapbundle = assetBundleCreateRequest.assetBundle;
            AssetBundleRequest bundleRequest = assetBundleCreateRequest.assetBundle.LoadAssetAsync<Material>("skybox");
            yield return bundleRequest;
            Skybox.Skybox = bundleRequest.asset as Material;
            cubemapbundle.Unload(false);
            Skybox.ApplySkybox();
            Skybox.ApplySkyboxParams();

            EnvironmentUpdateFlag = true;

            if (false == hideSkybox)
            {
                Camera.main.clearFlags = CameraClearFlags.Skybox;
            }

            cubemaploaded = true;
            cubemapbundle = null;
            bundleRequest = null;
            assetBundleCreateRequest = null;
            Resources.UnloadUnusedAssets();
            IsLoading = false;
            yield break;
        }

        ProceduralSkyboxManager ProceduralSkybox = new ProceduralSkyboxManager();
        SkyboxManager Skybox = new SkyboxManager();
        public GameObject probeGameObject = new GameObject("RealtimeReflectionProbe");
        private ProceduralSkyboxParams tempproceduralskyboxparams;
        private SkyboxParams tempskyboxparams;
        private GameObject LightsObj;
        ReflectionProbe probeComponent;
        private string[] CubeMapFileNames;
        private CharFemale CMfemale;
        public bool cameraCtrlOff;
        private Camera SubCamera;
        private FolderAssist cubemapFolder;

        private static ushort errorcode = 0;
        private Light BackDirectionalLight;
        private Light FrontDirectionalLight;
        private Light FrontDirectionalMapLight;
        private bool cubemaploaded = false;
        private bool hideSkybox = false;

        private bool Tonemapping_enabled = true;
        private TonemappingColorGrading.Tonemapper ToneMapper = TonemappingColorGrading.Tonemapper.ACES;
        private int SelectedToneMapper = 0;
        private float EV;
        private float eye_speed;
        private bool eye_enabled = false;
        private float eye_middleGrey;
        private float eye_min;
        private float eye_max;
        
        private TonemappingColorGrading ToneMappingManager;
        private Bloom BloomManager;
        private LensAberrations LensManager;

        private float previousambientintensity = 1f;

        private bool EnvironmentUpdateFlag = false;

        public static bool MainWindow = false;
        public float AmbientIntensity = 1f;

        private float CharaRotate = 0f;
        private Vector3 RotateValue;
        private bool autoRotate = false;
        private float autoRotateSpeed = 0.2f;
        private int selectedCubeMap;
        private int preveiousSelectedCubeMap;
        private int CMWaringWindowID = 14321;
        private int ErrorwindowID = 14322;
        private int windowID = 14333;
        private int ReflectionProbeResolution;
        private int ReflectionProbeRefreshRate;

        private Studio.CameraControl cameraCtrl;
        private int TabMenu;
        private bool FrontLightAnchor = true;
        private bool BackLightAnchor = true;
        private Vector3 FrontRotate;
        private Vector3 BackRotate;
        private bool IsLoading = false;
    }
}
